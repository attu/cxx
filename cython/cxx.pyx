# distutils: language = c++
# cython: c_string_type=str, c_string_encoding=ascii
# from cython.operator cimport dereference as deref
from libcpp.vector cimport vector
from libcpp.string cimport string
from libcpp.map cimport map
from cxxjson cimport byte, to_integer
from cxxjson cimport json as cppjson
from cxxjson cimport byte_stream
from cxxjson cimport null_t
from cxxjson cimport holds_alternative
from cxxjson cimport get as cget
from cxxmsgpack cimport msgpack as cppmsgpack
from cxxcbor cimport cbor as cppcbor
from libc.stdint cimport int64_t, uint8_t
cimport libcpp


cdef get(cppjson variant):
    if holds_alternative[int64_t](variant):
        return int(cget[int64_t](variant))
    if holds_alternative[double](variant):
        return float(cget[double](variant))
    if holds_alternative[libcpp.bool](variant):
        return bool(cget[libcpp.bool](variant))
    if holds_alternative[string](variant):
        return str(cget[string](variant))
    if holds_alternative[null_t](variant):
        return None
    if holds_alternative[byte_stream](variant):
        def gen():
            cdef byte_stream stream = cget[byte_stream](variant)
            for x in stream:
                yield to_integer[uint8_t](x)
        return bytes(list(gen()))
    if holds_alternative[vector[cppjson]](variant):
        def gen():
            cdef vector[cppjson] array = cget[vector[cppjson]](variant)
            for x in array:
                yield get(x)
        return list(gen())
    if holds_alternative[map[string, cppjson]](variant):
        def gen():
            cdef map[string, cppjson] dct = cget[map[string, cppjson]](variant)
            for item in dct:
                yield str(item.first), get(item.second)
        return {key: value for key, value in gen()}


cdef cppjson make_cxxjson(value):
    cdef cppjson variant
    cdef byte_stream data
    cdef vector[cppjson] array
    cdef map[string, cppjson] dictionary
    if value is None:
        variant = cppjson(null_t())
    elif isinstance(value, bool):
        variant = cppjson(< libcpp.bool > value)
    elif isinstance(value, int):
        variant = cppjson(< int64_t > value)
    elif isinstance(value, float):
        variant = cppjson(< double > value)
    elif isinstance(value, str):
        variant = cppjson(< string > value)
    elif isinstance(value, bytes):
        data.reserve(len(value))
        for x in value:
            data.push_back(byte(< uint8_t > x))
        variant = cppjson(data)
    elif isinstance(value, list):
        array.reserve(len(value))
        for x in value:
            array.push_back(make_cxxjson(x))
        variant = cppjson(array)
    elif isinstance(value, dict):
        for k, x in value.items():
            dictionary[< string > k] = make_cxxjson(x)
        variant = cppjson(dictionary)
    else:
        raise TypeError(f'unsupported type: from_{type(value).__name__}')
    return variant


cdef class msgpack:
    cdef cppmsgpack object

    @staticmethod
    def encode(value):
        cdef byte_stream data = cppmsgpack.encode(make_cxxjson(value))
        return bytes([< int > x for x in data])

    @staticmethod
    def decode(value):
        cdef byte_stream data
        data.reserve(len(value))
        for x in value:
            data.push_back(byte(< uint8_t > x))
        return get(cppmsgpack.decode(data))


cdef class cbor:
    cdef cppcbor object

    @staticmethod
    def encode(value):
        cdef byte_stream data = cppcbor.encode(make_cxxjson(value))
        return bytes([< int > x for x in data])

    @staticmethod
    def decode(value):
        cdef byte_stream data
        data.reserve(len(value))
        for x in value:
            data.push_back(byte(< uint8_t > x))
        return get(cppcbor.decode(data))
