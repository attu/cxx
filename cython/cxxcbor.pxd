from cxxjson cimport byte_stream
from cxxjson cimport json

cdef extern from "cxx/cbor.hpp" namespace "cxx":
    cdef cppclass cbor:
        @staticmethod
        byte_stream encode(const json&) except +

        @staticmethod
        json decode(const byte_stream&) except +
