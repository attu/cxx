from libc.stdint cimport int64_t, uint8_t
from libcpp cimport bool
from libcpp.map cimport map
from libcpp.string cimport string
from libcpp.vector cimport vector


cdef extern from "cstddef" namespace "std":
    cdef cppclass byte:
        byte()
        byte(uint8_t)

    cdef I to_integer[I](byte)


ctypedef vector[byte] byte_stream


cdef extern from "cxx/json.hpp" namespace "cxx::json":
    cdef cppclass null_t:
        null_t()


cdef extern from "cxx/json.hpp" namespace "cxx":
    cdef cppclass json:
        json()
        json(bool)
        json(const string & ) except +
        json(double)
        json(int64_t)
        json(const map[string, json]&)
        json(null_t)
        json(const byte_stream&)
        json(const vector[json]&)

    bool holds_alternative[T](const json&)

    const T& get[T](const json&)
