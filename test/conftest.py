from pytest import fixture
from itertools import combinations

values = [
    42, 7, 3.14, 2.71, 'lorem', 'ipsum', True, False, None, b'lorem', b'ipsum', [], [
        'lorem', 42, 3.14, True, None, ['ipsum']], dict(), {
            'lorem': 'ipsum', 'dolor': 42, 'sit': 3.14, 'amet': [
                True, None], 'consectetur':{
                    'adipisicing': 'elit'}}]
pairs = tuple(combinations(values, 2))


@fixture
def cxx():
    import cxx as ccxx
    return ccxx


@fixture
def cxxjson(cxx):
    return cxx.json


@fixture
def cxxmsgpack(cxx):
    return cxx.msgpack


@fixture
def cxxcbor(cxx):
    return cxx.cbor


@fixture
def pymsgpack():
    import msgpack
    msgpack.decode = lambda x: msgpack.unpackb(x, raw=False)
    msgpack.encode = lambda x: msgpack.packb(x, use_bin_type=True)
    return msgpack


@fixture
def pycbor():
    import cbor2
    cbor2.decode = lambda x: cbor2.loads(x)
    cbor2.encode = lambda x: cbor2.dumps(x)
    return cbor2


@fixture
def codec(request, cxxmsgpack, cxxcbor):
    if request.param == 'msgpack':
        return cxxmsgpack
    if request.param == 'cbor':
        return cxxcbor


@fixture
def reference(request, pymsgpack, pycbor):
    if request.param == 'msgpack':
        return pymsgpack
    if request.param == 'cbor':
        return pycbor
