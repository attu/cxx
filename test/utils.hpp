#pragma once
#include "inc/cxx/json.hpp"

namespace test::literals
{
  cxx::json::byte_stream operator"" _hex(char const* ptr, std::size_t size)
  {
    cxx::json::byte_stream bytes;
    if (size == 0) return bytes;
    bytes.reserve(size / 2);
    auto const last = ptr + size;
    while (ptr != last)
    {
      char const h = *ptr++;
      char const l = *ptr++;
      char const h_base = (h >= 'a' && h <= 'f') ? ('a' - 10) : '0';
      char const l_base = (l >= 'a' && l <= 'f') ? ('a' - 10) : '0';
      bytes.emplace_back((cxx::json::byte_stream::value_type(h - h_base) << 4) |
                         cxx::json::byte_stream::value_type(l - l_base));
    }
    return bytes;
  }
} // namespace test::literals
