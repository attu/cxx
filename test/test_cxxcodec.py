import pytest
from conftest import values
from binascii import hexlify


@pytest.mark.parametrize('codec, reference', [('msgpack', 'msgpack'), ('cbor', 'cbor')], indirect=True)
@pytest.mark.parametrize('value', values)
def test_cxxCodecHasReferenceCompatibileDataFormat(codec, reference, value):
    assert value == reference.decode(codec.encode(value))
    data = reference.encode(value)
    assert codec.decode(data) == value, f'failed to decode {hexlify(data)}'
